#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This is a simple implementation of the well known
game Tic Tac Toe.
It features a two level AI, network capabilities
as well as local player 1v1 and score tracking.
Settings are stored in the settings.yaml file.

Further information on this project can be found
in the ./analysis directory.

This class instances the game in a more readable way 
then using game.py as the main file.
"""

from core.game import Game

class TicTacToe(Game):
	def __init__(self):
		Game.__init__(self)

# execute game
TicTacToe = TicTacToe()
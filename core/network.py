"""
	Network module
	Uses public dropbox to store user scores.
	Connects players via telnet sockets.
"""

# import dropbox

class networkHandler:
	def __init__(self):
		pass

	def link(self):
		"""
			Connects to players.
		"""
		pass

	def die():
		"""
			Closes a player connection.
		"""
		pass

	def sync():
		"""
			Retrieves data from dropbox.
		"""
		pass

	def update():
		"""
			Writes data to dropbox.
		"""
		pass
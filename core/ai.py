"""
	AI module
	Set of functions to determine an optimal move.
	Also houses a representation of an AI player.
	Always returns board field index of the move.
"""

import math
import random
from core.player import Player
from core.logic import *

names = ["Leonardo", "Albert", "Stephen", "Jay", "JKWD"]

class AI(Player):
	def __init__(self, char, _id, name):
		Player.__init__(self, char, _id, name, True)

	"""
		Takes in the board situation and a player
		and tries - in descending order - all
		possible moves. It will return the first
		move possible, which is always the optimal
		move. 
		Can also be used on a human player, possibly
		to give a player a hint [to be implemented].
	"""

	def checkField(self, board, field, player):
		neighbours = {	"left" : self.checkLeft(board, field, player),
						"up" : self.checkUp(board, field, player),
						"right" : self.checkRight(board, field, player),
						"down" : self.checkDown(board, field, player)}
		return neighbours

	def checkLeft(self, board, field, player):
		try:
			if board[field[0]][field[1]-1] == player.getChar():
				return True
			else:
				return False
		except IndexError:
			return False

	def checkUp(self, board, field, player):
		try:
			if board[field[0]-1][field[1]] == player.getChar():
				return True
			else:
				return False
		except IndexError:
			return False

	def checkRight(self, board, field, player):
		try:
			if board[field[0]][field[1]+1] == player.getChar():
				return True
			else:
				return False
		except IndexError:
			return False

	def checkDown(self, board, field, player):
		try:
			if board[field[0]+1][field[1]] == player.getChar():
				return True
			else:
				return False
		except IndexError:
			return False

	def chooseMove(self, board, player=None):
		if player == None:
			player = self
		if self.tryWin(board, player) != False:
			return self.tryWin(board, player)
		if self.tryBlock(board, player) != False:
			return self.tryBlock(board, player)
		#if self.tryFork(board, player) != False:
		#	return self.tryFork(board, player)
		#self.tryBlockFork(board, players, player)
		if self.tryCenter(board, player) != False:
			return self.tryCenter(board, player)
		if self.tryOppositeCorner(board, player) != False:
			return self.tryOppositeCorner(board, player)
		if self.tryEmptyCorner(board, player) != False:
			return self.tryEmptyCorner(board, player)
		if self.tryEmptySide(board, player) != False:
			return self.tryEmptySide(board, player)

	def tryWin(self, board, player=None):
		if player == None:
			player = self
		for i in range(len(winningCombinations)):
			fieldIsPlayer=[];
			for j in range(len(winningCombinations[i])):
				index = twoDimensional(winningCombinations[i][j] - 1)
				if board[index[0]][index[1]] == player.getChar():
					fieldIsPlayer.append([index, "player"])
				elif board[index[0]][index[1]] == "":
					fieldIsPlayer.append([index, "empty"])
			pCount = 0
			emptyIndex = None
			for p in range(len(fieldIsPlayer)):
				if fieldIsPlayer[p][1] == "player":
					pCount+=1
				else:
					emptyIndex = fieldIsPlayer[p][0]
			if (pCount == 2 and emptyIndex != None):
				return emptyIndex
		return False


	def tryBlock(self, board, player=None):
		if player == None:
			player = self
		for i in range(len(winningCombinations)):
			fieldIsOpponent=[];
			for j in range(len(winningCombinations[i])):
				index = twoDimensional(winningCombinations[i][j] - 1)
				if board[index[0]][index[1]] != player.getChar() and board[index[0]][index[1]] != "":
					fieldIsOpponent.append([index, "opponent"])
				elif board[index[0]][index[1]] == "":
					fieldIsOpponent.append([index, "empty"])
			oCount = 0
			emptyIndex = None
			for o in range(len(fieldIsOpponent)):
				if fieldIsOpponent[o][1] == "opponent":
					oCount+=1
				else:
					emptyIndex = fieldIsOpponent[o][0]
			if (oCount == 2 and emptyIndex != None):
				return emptyIndex
		return False

	
	def tryFork(self, board, player=None):
		if player == None:
			player = self
		return getThird(forks, board, player)

	"""
	def tryBlockFork(self, board, players, player=None):
		if player == None:
			player = self
		return getThird(forks, board, getOpponent(player, players))
	"""

	def tryCenter(self, board, player=None):
		if player == None:
			player = self
		i = int(math.floor(len(board)/2))
		j = int(math.floor(len(board[i])/2))
		if board[i][j] == "":
			return [i, j]
		return False

	def tryOppositeCorner(self, board, player=None):
		if player == None:
			player = self
		for i in range(len(oppositeCorners)):
			field = twoDimensional(oppositeCorners[i][0])
			y = field[0]
			x = field[1]
			if board[y][x] != player.getChar() and board[y][x] != "":
				return [x, y]
			elif board[x][y] != player.getChar() and board[x][y] != "":
				return [y, x]
		return False

	def tryEmptyCorner(self, board, player=None):
		if player == None:
			player = self
		corners = [	[0, 0],
					[0, len(board[0])-1],
					[len(board)-1, 0],
					[len(board)-1, len(board[len(board)-1])-1]]
		emptyCorners = []
		for i in range(len(corners)):
			corner = corners[i]
			y = corner[0]
			x = corner[1]
			if board[y][x] == "":
				# @TODO return random empty corner if multiple
				emptyCorners.append([y, x])
		if len(emptyCorners) > 0:
			return random.choice(emptyCorners)
		return False

	def tryEmptySide(self, board, player=None):
		if player == None:
			player = self
		sides = [	[0, 1],
					[1, 0],
					[1, 2],
					[2, 1]	]
		emptySides = []
		for s in range(len(sides)):
			side = sides[s]
			y = side[0]
			x = side[1]
			if board[y][x] == "":
				# @TODO return random empty side if multiple
				emptySides.append([y, x])
		if len(emptySides) > 0:
			return random.choice(emptySides)
		return False
"""
    Game module
    Main Tic Tac Toe game class. Connects all modules.
    Loads configurations by list.
"""

import random
import time
from core.player import Player
import core.ai as AI
from core.logic import winningCombinations
from core.logic import twoDimensional

# settingsPath = "../settings.py"

class Game:
  def __init__(self):
    # set up board blueprint
    self.board = [["", "", ""], ["", "", ""], ["", "", ""]]

    # print welcome message
    print ( "\n\nWelcome to Tic Tac Toe!\n\n")

    # let player choose to play against AI, repeat if no valid answer is given
    while True:
      ai = raw_input("Do you want to play against AI? [y/n]: ")
      if ai == "y" or ai == "n":
        break

    # if player doesn't want to play against ai set up two players
    if ai == "n":
      firstPlayerName = raw_input("Player 1, choose your name: ")
      secondPlayerName = raw_input("Player 2, choose your name: ")
      # check if both names are the same, if so let player 2 choose another
      while secondPlayerName == firstPlayerName:
        print "You can't choose the same name as your opponent! Please choose another."
        secondPlayerName = raw_input("Player 2, choose your name: ")
      self.player1 = self.newPlayer("X", 1, firstPlayerName)
      self.player2 = self.newPlayer("O", 2, secondPlayerName)
    # else set up a player and an AI
    else:
      self.player1 = self.newPlayer("X", 1, raw_input("Choose your name: "))
      self.player2 = self.newAI("O", 2)

    # create an array of players
    self.players = [self.player1, self.player2]

    # Since the player going first is able to be unbeatable, the starting player is
    # chosen randomly, to make the game more fair.
    # Random uses the system time or (if available) the os hardware to generate
    # numbers suitable for cryptographic use. Hence they are suitable for our
    # purpose as well.
    # currentPlayer stores the current player's ID
    self.currentPlayer = self.players[random.randint(0, len(self.players) - 1)].getID()

    # print instructions
    print ( "\nThe board looks like this. Use the index\n" +
            "to place your mark at a square.\n\n" +
            "\t\t\t 1 | 2 | 3 \n" +
            "\t\t\t---*---*---\n" +
            "\t\t\t 4 | 5 | 6 \n" +
            "\t\t\t---*---*---\n" +
            "\t\t\t 7 | 8 | 9 \n")

    # tell which player goes first
    print self.playerByID(self.currentPlayer).getName(), " goes first!"

    # start game
    self.loop()

  def newPlayer(self, char, _id, playerName):
    """
        Create a new player instance.
    """
    player = Player(char, _id, playerName)
    return player

  def playerByID(self, _id):
    """
        Return the player with the matching ID.
    """
    for player in self.players:
      if _id == player.getID():
        return player

  def newAI(self, char, _id):
    """
        Create a new AI instance
    """
    # select a random AI name
    name = random.choice(AI.names)

    # instanciate AI
    ai = AI.AI(char, _id, name)
    return ai

  """
  def askNumber(self, low, high, question):
    number = None
    while number not in range(low, high):
      number = int(raw_input(question))
    return number
  """

  def printBoard(self):
    """
        Print the current board status.
    """
    print "\nThe board looks like this:\n"

    # iterate through rows
    for i in range(0, 3):
      print "\t\t\t",

      # iterate through columns
      for j in range(0, 3):

        # if a the field is occupied print the matching character, 
        # else print blank space
        if self.board[i][j] != "":
          print " " + self.board[i][j] + " ",
        else: 
          print "   ",
        if (j!=2):
          print "|",;
        else:
          print("");
      if (i!=2):
        print "\t\t\t--- * --- * ---"
      else:
        print ""

  def move(self, player):
    """
        Determine a players move.
    """
    # tell the players who's move it is
    print player.getName(), "'s move."

    # if the player is an AI let it determine its move 
    if player.isAI():
      # pause game for 0 - 2 seconds to make it look like AI is thinking
      time.sleep(random.randrange(0, 2))
      index = player.chooseMove(self.board) 
    # else let the player choose a field and validate his choice
    else:
      while True:
        try:
          # let player input a single digit number, convert it to 2d coordinates
          index = twoDimensional(int(raw_input("Choose the field you want to place your mark at: ")) - 1)
          # print a blank line
          print ""
          # if the field is not empty repeat
          if index not in self.legalMoves():
            # show the current board status for player to decide move
            self.printBoard()
            print("This move is not valid. Please try again!\n")
          else:
            break
        # if the input string is not a valid number repeat (e.g. 'wr34rsdfwe')
        except:
            # show the current board status for player to decide move
            self.printBoard()
            print("This move is not valid. Please try again!\n")

    # update field to be occupied by player's character
    if player.isAI():
      self.aiDebug(player)
    self.board[index[0]][index[1]] = player.getChar()

  def aiDebug(self, player):
    """
        Print AI move coordinates. 
    """
    print "AI win:", player.tryWin(self.board)
    print "AI block: ", player.tryBlock(self.board)
    print "AI fork: ", player.tryFork(self.board)
    #print "AI fork block: ", player.tryBlockFork(self.board, self.players)
    print "AI center: ", player.tryCenter(self.board)
    print "AI oppositeCorner ", player.tryOppositeCorner(self.board)
    print "AI emptyCorner: ", player.tryEmptyCorner(self.board)
    print "AI emptySide: ", player.tryEmptySide(self.board)

  def legalMoves(self):
    """
        Return a list of coordinates of empty board fields.
    """
    # return [x for x in range(len(self.board)) if self.board[x] == ""]
    legal = []
    for i in range(len(self.board)):
      for j in range(len(self.board[i])):
        if self.board[i][j] == "":
          legal.append([i, j])
    return legal

  """
  def logPlayerStats(self, player, score):
    if self.online:
        if not player.isAI():
          player.updateScore(score)
          if score >= 0:
            print player.getName(), "has been awarded ", str(score), " points."
          else:
            print player.getName(), "has been punished by ", str(score), " points."
  """

  """
  def printPlayerStats(self):
    print "Player stats:"
    if self.player1.getScore() == 1:
      print "\t\t\t", self.player1.getName(), "\t[ ", self.player1.getScore(), " point ]"
    else:
      print "\t\t\t", self.player1.getName(), "\t[ ", self.player1.getScore(), " points ]"      
    if not self.player2.isAI():
      if self.player2.getScore() == 1:
        print "\t\t\t", self.player2.getName(), "\t[ ", self.player2.getScore(), " point ]"
      else:
        print "\t\t\t", self.player2.getName(), "\t[ ", self.player2.getScore(), " points ]"
  """

  def checkEnd(self):
    """
        Check if a player has won or there's a draw. If not play another round.
    """
    if self.checkWin(self.player1):
      self.printBoard()
      print self.player1.getName(), " won!"
      """
      if self.online:
        self.player1.updateScore(5)
        print self.player1.getName(), "has been awarded 5 points."
        if not self.player2.isAI():
          print self.player2.getName(), "has been punished by 2 points."
          self.player2.updateScore(-2)
        self.printPlayerStats()
      """
      return True
    elif self.checkWin(self.player2):
      self.printBoard()
      print self.player2.getName(), " won!"
      """
      if self.online:
        if not self.player2.isAI():
          self.player2.updateScore(5)
          print self.player2.getName(), "has been awarded 5 points."
        self.player1.updateScore(-2)
        print self.player1.getName(), "has been punished by 2 points."
        self.printPlayerStats()
      """
      return True
    elif self.checkDraw():
      self.printBoard()
      print "It's a draw!"
      """
      if self.online:
        self.player1.updateScore(1)
        if not self.player2.isAI():
          self.player2.updateScore(1)
        print "Both players are granted 1 point."
        self.printPlayerStats()
      """
      return True
    else:
      return False

  def checkWin(self, player):
    """
        Check if a player set a row of three.
    """
    for i in range(0, len(winningCombinations)):
      met = True
      for j in range(0, 3):
        index = twoDimensional(winningCombinations[i][j]-1)
        if (self.board[index[0]][index[1]] != player.getChar()):
          met = False
      if met:
        return True
    return False

  def checkDraw(self):
    """
        Check if all field are occupied.
    """
    for i in range(len(self.board)):
      for j in range(len(self.board[i])):
        if (self.board[i][j] == ""):
          return False
    return True 

  def loop(self):
    """
        Play until the game has ended. Each round let a player move.
    """
    # play if the game is not over
    while(not(self.checkEnd())):
      #if not self.playerByID(self.currentPlayer).isAI():
        # show the current board status for human player to decide move
      self.printBoard()

      # let player move and change current player to the other's ID
      if(self.currentPlayer == self.player1.getID()):
        self.move(self.player1)
        self.currentPlayer = self.player2.getID()
      else:
        self.move(self.player2)
        self.currentPlayer = self.player1.getID()

    # if game is over ask player to play again, validate input
    while True:
      again = raw_input("Do you want to play again? [y/n]: ")
      if again == "y" or again == "n":
        break
    if again == "y":
      self.__init__()
    else:
      print "\nBye!\n"

# if the file is run as an executable create a game instance
if __name__ == "__main__":
  game = Game()
from Tkinter import *

class GraphicalInterface(Tk):
	def __init__(self, parent):
		Tk.__init__(self, parent)
		self.parent = parent
		self.initialize()
		self.mainloop()

	def initialize(self):
		self.title("Tic Tac Toe")
		self.canvas = Canvas
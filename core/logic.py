from math import floor

winningCombinations = 	[ 		
							[1, 2, 3],	# top
                			[4, 5, 6],	# middle horizontal
                			[7, 8, 9],	# bottom
                			[1, 4, 7],	# left vertical
                			[2, 5, 8],	# middle vertical
                			[3, 6, 9],	# right vertical
                			[1, 5, 9],	# diagonal left right
                			[3, 5, 7]	# diagonal right left
                		]

forks =                 [
                            [1, 2, 4],  # top left corner
                            [2, 3, 6],  # top right corner
                            [4, 7, 8],  # bottom left corner
                            [6, 8, 9],  # bottom right corner
                            [2, 4, 5],  # middle top left
                            [2, 5, 6],  # middle top right
                            [4, 5, 8],  # middle bottom left
                            [5, 6, 8]   # middle bottom right
                        ]

oppositeCorners =   [
                        [1, 9],
                        [3, 7]
                    ]

def playerByID(_id, players):
    for player in players:
      if _id == player.getID():
        return player

def playerByChar(char, players):
    for player in players:
        if char == player.getChar():
            return player

def twoDimensional(index):
    i = int(floor(index / 3))
    j = index % 3
    return [i, j]

def getThird(array, board, player):
    for i in range(len(array)):
        fieldIsPlayer=[];
        for j in range(len(array[i])):
            index = twoDimensional(array[i][j] - 1)
            if board[index[0]][index[1]] == player.getChar():
                fieldIsPlayer.append([index, "player"])
            elif board[index[0]][index[1]] == "":
                fieldIsPlayer.append([index, "empty"])
        pCount = 0
        emptyIndex = None
        for p in range(len(fieldIsPlayer)):
            if fieldIsPlayer[p][1] == "player":
                pCount+=1
            else:
                emptyIndex = fieldIsPlayer[p][0]
        if (pCount == 2 and emptyIndex != None):
            return emptyIndex
    return False

def getOpponent(player, players):
    if player.getChar() == "X":
        return playerByChar("O", players)
    else:
        return playerByChar("X", players)
""" 
	Player module
	A player's representation.
"""

# from core.game import settingsPath

class Player():
	def __init__(self, char, _id, name, ai=False):
		self.char = char
		self.name = name
		self.id = _id
		self.score = 0
		self.ai = ai
		self.sync()

	def getChar(self):
		return self.char

	def getName(self):
		return self.name

	def getID(self):
		return self.id

	def isAI(self):
		return self.ai

	def getScore(self):
		return self.score

	def setScore(self, score):
		self.score = score

	def updateScore(self, score):
		if (self.score + score) < 0:
				self.score = 0
		else:
			self.score += score

	def openFile(self, path):
		file = open(path, 'r+')
		return file

	def sync(self):
		"""
			Loads player score.
		"""
		pass

	def update(self):
		"""
			Writes player score to file.
		"""
		pass